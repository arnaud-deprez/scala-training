package be.arndep.scala.circuit

/**
	* Created by arnaud.deprez on 16/01/16.
	*/
trait Parameters {
	def InverterDelay = 2
	def AndGateDelay = 3
	def OrGateDelay = 5
}
